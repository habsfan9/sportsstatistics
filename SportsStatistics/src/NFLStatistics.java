import java.util.Scanner;
public class NFLStatistics 
{
	public String category;
	public String leader;
	public String statValue;
	
	public void getCategory()
	{
		int categoryChosen;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Which statistic do you want to look up?");
		System.out.println("Enter <1> for Passing Touchdowns (QB), <2> for Total Rushing Yards, or <3> for Games Played.");
		category = keyboard.nextLine();
		
		if ((category.equals("1")) || (category.equals("2")) || (category.equals("3")))
		{
			categoryChosen = Integer.parseInt(category);
			
			if (categoryChosen == 1)
				category = ("Passing Touchdowns (QB)");
			else if (categoryChosen == 2)
				category = ("Total Rushing Yards");
			else
				category = ("Games Played");
		}
		else
		{	
			keyboard = new Scanner(System.in);
			System.out.println("Please enter <1> for Passing Touchdowns (QB), <2> for Total Rushing Yards, or <3> for Games Played.");
			category = keyboard.nextLine();
			
			while ((!category.equals("1")) && (!category.equals("2")) && (!category.equals("3")))
			{
				keyboard = new Scanner(System.in);
				System.out.println("Enter <1> for Passing Touchdowns (QB), <2> for Total Rushing Yards, or <3> for Games Played.");
				category = keyboard.nextLine();
			}
			
			categoryChosen = Integer.parseInt(category);
			
			if (categoryChosen == 1)
				category = ("Passing Touchdowns (QB)");
			else if (categoryChosen == 2)
				category = ("Total Rushing Yards");
			else
				category = ("Games Played");
		}
		
	}
	
	public void getLeader()
	{
		if (category.equals("Games Played"))
			leader = "Morten Andersen (Kicker)";
		else if (category.equals("Total Rushing Yards"))
			leader = "Emmitt Smith";
		else
			leader = "Brett Favre";
	}
	
	public void getstatValue()
	{
		if (category.equals("Games Played"))
			statValue = ("382");
		else if (category.equals("Total Rushing Yards"))
			statValue = ("18,355");
		else
			statValue = ("508");
	}
	
	public void writeOutput()
	{
		System.out.println("Statistical category: " + category);
		System.out.println("Statistical leader: " + leader);
		System.out.println("Number of " + category + ": " + statValue);
	}
}
