import java.util.Scanner;
public class NBAStatistics 
{
	public String category;
	public String leader;
	public String statValue;
	
	public void getCategory()
	{
		int categoryChosen;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Which statistic do you want to look up?");
		System.out.println("Enter <1> for Total Points Scored, <2> for Assists, or <3> for Games Played.");
		category = keyboard.nextLine();
		
		if ((category.equals("1")) || (category.equals("2")) || (category.equals("3")))
		{
			categoryChosen = Integer.parseInt(category);
			
			if (categoryChosen == 1)
				category = ("Total Points Scored");
			else if (categoryChosen == 2)
				category = ("Assists");
			else
				category = ("Games Played");
		}
		else
		{	
			keyboard = new Scanner(System.in);
			System.out.println("Please enter <1> for Total Points Scored, <2> for Assists, or <3> for Games Played.");
			category = keyboard.nextLine();
			
			while ((!category.equals("1")) && (!category.equals("2")) && (!category.equals("3")))
			{
				keyboard = new Scanner(System.in);
				System.out.println("Enter <1> for Total Points Scored, <2> for Assists, or <3> for Games Played.");
				category = keyboard.nextLine();
			}
			
			categoryChosen = Integer.parseInt(category);
			
			if (categoryChosen == 1)
				category = ("Total Points Scored");
			else if (categoryChosen == 2)
				category = ("Assists");
			else
				category = ("Games Played");
		}
		
	}
	
	public void getLeader()
	{
		if (category.equals("Games Played"))
			leader = "Robert Parish";
		else if (category.equals("Assists"))
			leader = "John Stockton";
		else
			leader = "Kareem Abdul-Jabbar";
	}
	
	public void getstatValue()
	{
		if (category.equals("Games Played"))
			statValue = ("1,611");
		else if (category.equals("Assists"))
			statValue = ("15,806");
		else
			statValue = ("38,387");
	}
	
	public void writeOutput()
	{
		System.out.println("Statistical category: " + category);
		System.out.println("Statistical leader: " + leader);
		System.out.println("Number of " + category + ": " + statValue);
	}
}
