/*
 Output: There are statistics for the NHL, NBA, & NFL. Which sport do you want to look at?"
 Ask for user input
 Store user input into sport
 
 if sport equals 1, 2, 3, or 0, convert sport into an int and store into sportChosen
 	if sportChosen equals 1
		sport = "NHL"
	else if sportChosen equals 2
		sport = "NBA"
	else if sportChosen equals 3
		sport = "NFL"
	else
		Exit program
		
if sport equals "NHL"
	Run NHLstat
	Ask if user wants to look up another statistic
		if yes, re-run NHLstat
		if no, ask if user wants to choose another sport
			if yes, re-run main method
			if no, exit program
if sport equals "NBA"
	Do same as for NHL but with NBAstat
if sport equals "NFL"
	Do same as for NHL but with NFLstat



 */
import java.util.Scanner;
public class SportsStatistics 
{
	public static void main(String[] args)
	{
		String changeQuestion;
		do
		{
			NHLStatistics NHLstat = new NHLStatistics();
			NBAStatistics NBAstat = new NBAStatistics();
			NFLStatistics NFLstat = new NFLStatistics();
			
			String sport;
			int sportChosen;
			
			Scanner keyboard = new Scanner(System.in);
			System.out.println("There are statistics for the NHL, NBA, & NFL. Which sport do you want to look at?");
			System.out.println("Enter <1> for NHL, <2> for NBA, or <3> for NFL. Enter <0> to exit.");
			sport = keyboard.nextLine();
					
			if ((sport.equals("1")) || (sport.equals("2")) || (sport.equals("3")) || (sport.equals("0")))
			{
				sportChosen = Integer.parseInt(sport);
				
				if (sportChosen == 1)
					sport = ("NHL");
				else if (sportChosen == 2)
					sport = ("NBA");
				else if (sportChosen == 3)
					sport = ("NFL");
				else
					System.exit(0);
			}
			else
			{	
				keyboard = new Scanner(System.in);
				System.out.println("Please enter <1> for NHL, <2> for NBA, or <3> for NFL. Enter <0> to exit.");
				sport = keyboard.nextLine();
				
				while ((!sport.equals("1")) && (!sport.equals("2")) && (!sport.equals("3")))
				{
					keyboard = new Scanner(System.in);
					System.out.println("Enter <1> for NHL, <2> for NBA, or <3> for NFL. Enter <0> to exit.");
					sport = keyboard.nextLine();
				}
				
				sportChosen = Integer.parseInt(sport);
				
				if (sportChosen == 1)
					sport = ("NHL");
				else if (sportChosen == 2)
					sport = ("NBA");
				else if (sportChosen == 3)
					sport = ("NFL");
				else
					System.exit(0);
			}
			
			if (sport.equalsIgnoreCase("NHL"))
			{
				String question;
				
				do
			    {
					System.out.println("NHL Statistics");

					NHLstat.getCategory();
					NHLstat.getLeader();
					NHLstat.getstatValue();
					NHLstat.writeOutput();
				
					System.out.println("Do you want to look up another NHL statistic? If so, enter <yes>.");
					keyboard = new Scanner(System.in);
					question = keyboard.nextLine();
				
					
			    } while (question.equalsIgnoreCase("yes"));
			}
			else if (sport.equalsIgnoreCase("NBA"))
			{
				String question;
				
				do
			    {
					System.out.println("NBA Statistics");

					NBAstat.getCategory();
					NBAstat.getLeader();
					NBAstat.getstatValue();
					NBAstat.writeOutput();
				
					System.out.println("Do you want to look up another NBA statistic? If so, enter <yes>.");
					keyboard = new Scanner(System.in);
					question = keyboard.nextLine();
				
					
			    } while (question.equalsIgnoreCase("yes"));
			}
			else
			{
				String question;
				
				do
			    {
					System.out.println("NFL Statistics");

					NFLstat.getCategory();
					NFLstat.getLeader();
					NFLstat.getstatValue();
					NFLstat.writeOutput();
				
					System.out.println("Do you want to look up another NFL statistic? If so, enter <yes>.");
					keyboard = new Scanner(System.in);
					question = keyboard.nextLine();
				
					
			    } while (question.equalsIgnoreCase("yes"));
			}
			
			System.out.println("Do you want to change sports? If so, enter <yes>. Enter anything else to exit.");
			keyboard = new Scanner(System.in);
			changeQuestion = keyboard.nextLine();
			
			if (!changeQuestion.equalsIgnoreCase("yes"))
			{
				System.out.print("Good Bye\n");
				System.exit(0);
			}
			
		} while (changeQuestion.equalsIgnoreCase("yes"));
	}
}
